# protect4less_django
This is a sample django project to manage gallery with multiple images.

## Getting Started

This project works on **Python 3+** and Django 2+.

Run following commands to start project:

```
pip install -r requirements.txt
 
python manage.py makemigrations posts
python manage.py migrate
python manage.py runserver
```

## Feutures

1. Create gallery and upload multiple images.
2. Update and delete single gallery images.

Added Screenshots within ``` Scrennshots/``` folder.


