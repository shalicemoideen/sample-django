from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .models import Projects, Configuration

def blog_view(request):
    return render(request, 'blog.html' )



def create_ticket_view(request):
    if request.method == 'POST':
        import ipdb;ipdb.set_trace()
        tool = request.POST.get('tool')
        endpoint = request.POST.get('endpoint')
        username = request.POST.get('username')
        password = request.POST.get('password')
        authtype = request.POST.get('authtype')
        selected_projects = request.POST.get('selected_projects')
        selected_projects = selected_projects.split(",")
        
        conf = Configuration.objects.create(
            tool=tool,
            endpoint=endpoint,
            username=username,
            password=password,
            authtype=authtype,
        )

        for pid in selected_projects:
            projectobj= get_object_or_404(Projects, id=pid)
            conf.projects.add(projectobj)

    return render(request, 'create-ticket.html')
   