from django.contrib import admin
from .models import Projects, Configuration

admin.site.register(Projects)
admin.site.register(Configuration)