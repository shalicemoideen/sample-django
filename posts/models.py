from django.db import models

class Projects(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % (self.name )

class Configuration(models.Model):
	PWD = 'PD'
	TKN = 'TK'
	AUTH_TYPES = [
        (PWD, 'Password'),
        (TKN, 'Token'),
    ]

	tool = models.CharField(max_length=250)
	endpoint = models.CharField(max_length=250)
	username = models.CharField(max_length=250)
	password = models.CharField(max_length=250)
	authtype = models.CharField(
		max_length=2,
		choices=AUTH_TYPES,
		default=PWD,
	)
	projects = models.ManyToManyField(Projects,blank=True)

	def __str__(self):
		return "%s" % (self.tool)